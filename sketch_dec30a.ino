#include "LedControl.h"

LedControl lc=LedControl(D8,D6,D7,6);

int MODULE_COUNT = 6;
int INFO_MODULE = 0;
int DISPLAY_INTENSITY = 8;

byte buttonsPressed = 0;
byte buttonOnePressed = 0;
byte buttonTwoPressed = 0;
byte buttonThreePressed = 0;
byte buttonFourPressed = 0;
byte buttonFivePressed = 0;

enum { 
  waitingForButtonDown,
  updateNumberDisplays,
  updateAverageDisplay,
  waitingForButton1Up,
  waitingForButton2Up,
  waitingForButton3Up,
  waitingForButton4Up,
  waitingForButton5Up
};

int state = waitingForButtonDown;

void setup() {
  initializeButtons();
  initializeBoard();
  Serial.begin(9600);   
}

void loop() {
  if(state == waitingForButtonDown) {
    waitForButtonDown();
  }

  if(state == waitingForButton1Up) {
    if(checkButtonAndSet(D0, LOW, updateNumberDisplays)) {
      buttonOnePressed++;
      buttonsPressed++;      
    }
  } 
  
  if(state == waitingForButton2Up) {
    if(checkButtonAndSet(D1, LOW, updateNumberDisplays)) {
      buttonTwoPressed++;
      buttonsPressed++; 
    }
  }
     
  if(state == waitingForButton3Up) {
    if(checkButtonAndSet(D2, LOW, updateNumberDisplays)) {
      buttonThreePressed++;
      buttonsPressed++; 
    }
  } 

  if(state == waitingForButton4Up) {
    if(checkButtonAndSet(D3, HIGH, updateNumberDisplays)) {
      buttonFourPressed++;
      buttonsPressed++;
    }
  }  

  if(state == waitingForButton5Up) {
    if(checkButtonAndSet(D5, LOW, updateNumberDisplays)) {
      buttonFivePressed++;
      buttonsPressed++;
    }
  } 

  if(state == updateNumberDisplays) {
    updateDisplays();
    state = updateAverageDisplay;
  }

  if(state == updateAverageDisplay) {
    calculateNewAverage();
    state = waitingForButtonDown;
  }
}

void initializeButtons() {
   pinMode(D0, INPUT_PULLUP);
   pinMode(D1, INPUT);   
   pinMode(D2, INPUT);   
   pinMode(D3, INPUT);   
   pinMode(D5, INPUT);
}

void initializeBoard() {
  for(int number = INFO_MODULE; number < MODULE_COUNT; number++) {
    lc.shutdown(number, false);
    lc.setIntensity(number, DISPLAY_INTENSITY);
    lc.clearDisplay(number); 
  }
  lc.setRow(INFO_MODULE, 0, B11100111);
  lc.setRow(INFO_MODULE, 1, B10111110);
  lc.setRow(INFO_MODULE, 2, B11011011);
  lc.setRow(INFO_MODULE, 3, B10110111);
  updateDisplays();
}

void waitForButtonDown() {
  checkButtonAndSet(D0, HIGH, waitingForButton1Up);
  checkButtonAndSet(D1, HIGH, waitingForButton2Up);
  checkButtonAndSet(D2, HIGH, waitingForButton3Up);
  checkButtonAndSet(D3, LOW, waitingForButton4Up);
  checkButtonAndSet(D5, HIGH, waitingForButton5Up);
}

bool checkButtonAndSet(int pin, int value, int newState) {
  if(digitalRead(pin) == value) {
    char log[8];
    sprintf(log, "%d -> %d", pin, value);
    Serial.println(log);
    state = newState;
    delay(100);
    return true;
  }
  return false;
}

void updateDisplays() {
  updateDisplay(1, buttonOnePressed);
  updateDisplay(2, buttonTwoPressed);
  updateDisplay(3, buttonThreePressed);
  updateDisplay(4, buttonFourPressed);
  updateDisplay(5, buttonFivePressed);
}

void updateDisplay(int displayNumber, byte value) {
  byte cent = value/100;
  value -= cent*100;    
  byte single = value%10;
  byte deca = value/10;
  lc.setDigit(displayNumber, 1, single, false);
  lc.setDigit(displayNumber, 0, deca, false);
}

void calculateNewAverage() {
  int allValues = (buttonOnePressed * -2)
    + (buttonTwoPressed * -1) 
    + buttonFourPressed
    + (buttonFivePressed * 2);

  int average = (allValues*100)/buttonsPressed;

  bool showNegative = average < 0;
  
  Serial.println(average);
  short cent = average/100;
  Serial.println(cent);
  average -= cent*100;   
  Serial.println(average); 
  short deca = average/10;
  Serial.println(deca);
  short single = average%10;
  Serial.println(single);  

  if(showNegative) {
    lc.setChar(INFO_MODULE, 0, '-', false);  
    lc.setDigit(INFO_MODULE, 1, cent*-1, true);
    lc.setDigit(INFO_MODULE, 2, deca*-1, false);
    lc.setDigit(INFO_MODULE, 3, single*-1, false);      
  } else {
    lc.setRow(INFO_MODULE, 0, B00000000);
    lc.setDigit(INFO_MODULE, 1, cent, true);
    lc.setDigit(INFO_MODULE, 2, deca, false);
    lc.setDigit(INFO_MODULE, 3, single, false);   
  }
}

